main:
        mov sp, #100
        mov r0, #10
        bl fib_rec_a
end:
        b end

fib_rec_a:
        sub sp, sp, #16
        str lr, [sp]
        cmp r0, #2
        blt fib_end

rec:
        str r0, [sp, #4]
        sub r0, r0, #1
        bl fib_rec_a
        str r0, [sp, #8]
        ldr r0, [sp, #4]
        sub r0, r0, #2
        bl fib_rec_a
        ldr r1, [sp, #8]
        add r0, r0, r1

fib_end:
        ldr lr, [sp]
        add sp, sp, #16
        bx lr
